import MQTT, {type MQTTError, Message} from "paho-mqtt";
import {browser} from '$app/environment';
import {get} from 'svelte/store';
import {powerStore} from "$lib/stores";
import {generateString} from "./utils";

const host = "test.mosquitto.org";
const port = 8080
const clientId = generateString(5)
const destination = "/power"

if (browser) {
  const client = new MQTT.Client(host, port, clientId);
  console.log(client)

  const onSuccess = () => {
    client.subscribe(destination);
    const message = new MQTT.Message(String(get(powerStore)));
    message.destinationName = destination;
    client.send(message);
  };

  const onFailure = () => {
    console.log("failure")
  };

  const tryReconnect = () => {
    client.subscribe(destination)
    setTimeout(() => tryReconnect, 3000);
  }

  const onConnectionLost = (responseObject: MQTTError) => {
    if (responseObject.errorCode) {
      tryReconnect();
    }
  };


  const onMessageArrived = (message: Message) => {
    console.log("onMessageArrived:" + message.payloadString);
    powerStore.set(Number(message.payloadString))
  };

  powerStore.subscribe(() => {
    if (client.isConnected()) {
      const message = new MQTT.Message(String(get(powerStore)));
      message.destinationName = destination;
      client.send(message);
    }
  })


  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived;
  client.connect({onSuccess, onFailure})

}
